package psu.surin;

import java.util.stream.IntStream;

public class BitNeuron {

    private float[] theW;
    private float bias;
    private BitUnit[] units;

    public BitNeuron() {
        units = new BitUnit[Settings.getCountA()];
        for (BitUnit unit : units) {
            unit.setTheA(Settings.getRndBitSet());
        }
    }

    private static float sigmoid(float x) {
        return (float) (1 / (1 + Math.pow(Math.E, (-1 * x))));
    }

    public float feedforward(boolean[] theS) {
        return sigmoid(
                (float) IntStream.range(0, units.length)
                        .mapToDouble(i -> units[i].feedforward(theS) * theW[i])
                        .sum() + bias
        );
    }

    public void lesson() {

    }

}
