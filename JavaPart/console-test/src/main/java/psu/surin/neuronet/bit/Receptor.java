package psu.surin.neuronet.bit;

import psu.surin.neuronet.settings.SettingsBit;

import java.util.BitSet;
import java.util.Random;

public class Receptor {

    private BitSet reactiveFilter;
    private int threshold;

    public Receptor() {
        threshold = SettingsBit.thresholdReceptor;

        reactiveFilter = new BitSet();
        Random random = new Random();
        for (int i = 0; i < SettingsBit.lengthInVector; i++) {
            if (random.nextBoolean())
                reactiveFilter.set(i);
        }
    }

    public boolean feedforward(BitSet inVector) {
        BitSet filter = (BitSet) reactiveFilter.clone();
        filter.and(inVector);
        return filter.cardinality() >= threshold;
    }

}
