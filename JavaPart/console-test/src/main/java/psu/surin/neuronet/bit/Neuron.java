package psu.surin.neuronet.bit;

import psu.surin.neuronet.settings.SettingsBit;

import java.util.Arrays;
import java.util.BitSet;

public class Neuron {

    private Receptor[] receptors;
    private double[] importanceReceptors;
    private int threshold;

    public Neuron() {
        threshold = SettingsBit.thresholdNeuron;
        receptors = new Receptor[SettingsBit.countReceptor];
        Arrays.fill(receptors, new Receptor());
        importanceReceptors = new double[SettingsBit.countReceptor];
        Arrays.fill(importanceReceptors, 0);
    }

    public boolean feedforward(BitSet inVector) {
        double sum = 0;
        for (int i = 0; i < receptors.length; i++) {
            if (receptors[i].feedforward(inVector))
                sum += importanceReceptors[i];
        }
        return sum >= threshold;
    }

}
