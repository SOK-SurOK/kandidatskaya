package psu.surin.neuronet.bit;

import psu.surin.neuronet.settings.SettingsBit;

import java.util.Arrays;
import java.util.BitSet;

public class Layer {

    private Neuron[] neurons;

    public Layer() {
        neurons = new Neuron[SettingsBit.countNeuron];
        Arrays.fill(neurons, new Neuron());
    }

    public BitSet feedforward(BitSet inVector) {
        BitSet outVector = new BitSet();
        for (int i = 0; i < neurons.length; i++) {
            outVector.set(i, neurons[i].feedforward(inVector));
        }
        return outVector;
    }

}
