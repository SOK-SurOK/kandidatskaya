package psu.surin.neuronet.settings;

public class SettingsBit {
    public static int thresholdNeuron = 2;
    public static int thresholdReceptor = 2;
    public static int countReceptor = 5;
    public static int lengthInVector = 8;
    public static int countNeuron = 3;
}
