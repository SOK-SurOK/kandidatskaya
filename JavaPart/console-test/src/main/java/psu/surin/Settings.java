package psu.surin;

import java.util.BitSet;
import java.util.Random;

public class Settings {

    private static int thresholdA;
    private static int lengthS;
    private static int countA;

    public static void update(int lengthS, int countA, int thresholdA) {
        System.out.println("update settings...");
        Settings.lengthS = lengthS;
        Settings.countA = countA;
        Settings.thresholdA = thresholdA;
        System.out.println("settings updated");
    }

    public static int getThresholdA() {
        return thresholdA;
    }

    public static int getLengthS() {
        return lengthS;
    }

    public static int getCountA() {
        return countA;
    }

    public static boolean[] intToBoolean(int[] x) {
        int length = x.length;
        boolean[] arr = new boolean[length];
        for (int i = 0; i < length; i++)
            arr[i] = x[i] == 0;
        return arr;
    }

    public static BitSet getRndBitSet() {
        Random random = new Random();
        BitSet bitSet = new BitSet(lengthS);
        for (int i = 0; i < lengthS; i++) {
            if (random.nextBoolean())
                bitSet.set(i);
        }
        return bitSet;
    }

}
