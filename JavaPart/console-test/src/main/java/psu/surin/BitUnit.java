package psu.surin;

import java.util.BitSet;

public class BitUnit {

    private BitSet theA;
    private float[] Score;

    public void setTheA(BitSet theA) {
        this.theA = theA;
    }

    public byte feedforward(boolean[] theS) {
        return (byte) (
                theA.stream()
                        .filter(setBitId -> theS[setBitId])
                        .count() > Settings.getThresholdA() ? 1 : 0
        );
    }

    public boolean feedforward(BitSet x) {
        return theA.stream().filter(x::get).count() > Settings.getThresholdA();
    }

}
