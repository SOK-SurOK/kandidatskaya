package psu.surin.theopencv;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

@Slf4j
public class FilesPrepare {

    private final ClassLoader mainClassLoader;

    private final Path outputDir;

    public FilesPrepare(ClassLoader mainClassLoader, Path outputDir) {
        this.mainClassLoader = mainClassLoader;
        this.outputDir = outputDir;
    }

    public String prepareFile(String resourcePath) throws IOException {
        File targetFile = new File(outputDir.toFile().getAbsolutePath() + Path.of("/" + resourcePath));
        if (!targetFile.exists()) {
            try (InputStream inputStream = mainClassLoader.getResourceAsStream(resourcePath)) {
                assert inputStream != null;
                log.info("create file - {}", targetFile.getAbsolutePath());
                FileUtils.copyInputStreamToFile(inputStream, targetFile);
            }
        }
        return targetFile.getAbsolutePath();
    }

}
