import lombok.extern.slf4j.Slf4j;
import nu.pattern.OpenCV;
import psu.surin.theopencv.FilesPrepare;
import psu.surin.theopencv.TheOpenCV;

import java.io.IOException;
import java.nio.file.Path;

@Slf4j
public class Main {

    public static void main(String[] args) throws IOException {

        OpenCV.loadShared();  // инициализация opencv (необходима)

//        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        Path outputDir = Path.of("console-test", "tmp");
        log.info("outputDir - {}", outputDir.toFile().getAbsolutePath());
        FilesPrepare filesPrepare = new FilesPrepare(Main.class.getClassLoader(), outputDir);

        String fileIn = filesPrepare.prepareFile("dataOpenCV/TestImg-300x300.jpg");
        String fileHaar = filesPrepare.prepareFile("haarcascades/haarcascade_frontalface_alt.xml");
        String pathOut = outputDir.toFile().getAbsolutePath() + Path.of("/test1.png");

        TheOpenCV.classifierImage(fileIn, fileHaar, pathOut);
    }

}
