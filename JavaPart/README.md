# Структура проекта
Версия: JAVA 11

* api - API для взаимодействия с подсистемами
* modules - Подсистемы

### Настройка проекта
1. Открыть ``Maven Settings``
2. Выбрать ``Use maven wrapper`` в `Maven home path`
3. Вызвать ``clean compile`` на модуле `root` для генерации proto файлов

### Создание новой подсистемы
1. В модуле `modules` выбрать `New` -> `Module...`
2. Создать по шаблону `Quarkus` (или `mvn io.quarkus.platform:quarkus-maven-plugin:{ВЕРСИЯ_ПЛАГИНА(пример 2.6.1)}:create -DprojectGroupId=... -DprojectArtifactId=...`)
3. Выбрать необходимые расширения (gRPC)
4. Удалить директорию `.mvn`
5. Удалить файлы `mvnw` и `mvnw.cmd`
6. Удалить директорию `src/main/resources/META-INF/resources`
7. При необходимости удалить лишние Dokerfile в `src/main/docker`
8. Указать в `README.md` краткое описание подсистемы
9. В `pom.xml` указать родительский модуль `modules`
10. В `pom.xml` `modules` указать дочерние модули
11. При необходимости отключить http сервер ``quarkus.http.host-enabled=false``
12. Указать порт для переопределения из переменных окружения и по умолчанию `quarkus.grpc.server.port=${PORT}`
13. Закоммитить изменения

### Создание конфигурации для запуска сервиса
1. Открыть ``Run/Debug Configuration`` (Add Configuration...)
2. Создать новую конфигурацию `+` (Add new configuration)
3. Выбрать шаблон ``Quarkus``
4. В `name` указать название подсистемы
5. В `Application module` выбрать модуль подсистемы
6. В `Before launch` удалить `Build`
7. В `Before launch` добавить `Run maven goal`
8. В `Working directory` Выбрать модуль для компиляции
9. В `Command line` указать `clean compile`
10. Поставить галочку в `Store as project file`
11. `Done`
12. `Apply`
13. Во вкладке `Runner` добавить `Environment variables` хост и порт
14. `Ok`
15. Проверить работоспособность
16. Закоммитить изменения

### Подключение подсистемы к API
1. Запустить подсистему
2. Скопировать нужную версию протокола `.proto` в модуль API
3. Указать хост и порт для подключения к подсистеме в модуле API
4. Запустить модуль API


### Пример добавления расширения quarkus в подсистему
1. Выбрать модуль(модули)
2. Выполнить ```mvn quarkus:add-extension -Dextensions=resteasy-jackson,rest-client```