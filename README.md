# Кандидатская работа
## Методы искусственного интеллекта в идентификации человека и его намерений

### Микросервисы:
1. Внешний api
2. (Возможно) отдельный quarkus модуль распределения 
3. Java интеропритация алгоритма Редозубова-Сурина
4. Python модуль приема изображений с камеры
5. Python модуль обработки изображения нейросетями
6. (Возможно) PostgreSQL бд

### Фронтенд:
1. Сервер nginx
2. Python Ursina Engine

### Перцептрон

![Тут должно быть изображение](doc-images/перцепторн.bmp)

Стандартный метод обучения перцептрона – метод коррекции ошибки. Этот метод относится к виду обучения с учителем. 
Вес связи остается неизменным до тех пор, пока реакция перцептрона остается верной. 
При возникновении неправильной реакции вес изменяется на единицу, а знак становится противоположным знаку ошибки.

Ниже описан поэтапный алгоритм обучения с учителем:
1. Инициализируются случайными значениями пороги A-элементов и устанавливаются связи S-A (связи далее не будут изменяться). 
    Начальные веса V = 0.
2. Предъявляем примеры (входные значения и ожидаемый выход):
   1. Правильный объект: Веса у тех A-элементов, которые преодолеют порог и связаны с возбужденным R-элементом, увеличиваем на единицу.
   2. Неправильный объект: Веса у тех A-элементов, которые преодолеют порог и связаны с возбужденным R-элементом, уменьшаем на единицу.
   Однако, у нас нет неправильных оюъектов (у нас есть другие), поэтому будем уменьшать у тех, что возбудились, хотя не должны были.
   
Розенблатт описал и доказал теорему сходимости, которая показывает, что элементарный перцептрон,
который обучается по такому методу, всегда приведет к достижению решения поставленной задачи за конечный промежуток времени.

Также существует и метод обучения без учителя. Веса всех активных A-элементов, которые ведут к R-элементу,
изменяются на одинаковую некоторую величину, а веса всех неактивных связей остаются неизменными.
 

