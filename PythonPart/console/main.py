import math
import time
import unittest

import numpy as np

from src.dataExample import DataExampleBool, DataExampleFloat
from src.dataNeuron import DataNeuron
from src.dataVecXy import DataVecXy
from src.neuro import separate, train
from src.neuroNet import NeuroNet
from src.neuroNetHide import NeuroNetHide
from src.neuroNetHideBin import NeuroNetHideBin
from src.neuroNetHideUsual import NeuroNetHideUsual
from src.settingsTrain import SettingsTrain


def get_test_suites() -> unittest.TestSuite:
    return unittest.TestLoader().discover(start_dir='testMy', pattern="test*.py")


def get_x_y_arr(x_vec_arr: np.ndarray, y_vec_arr: np.ndarray) -> np.ndarray:
    """
    :param x_vec_arr: np.bool[:, :]
    :param y_vec_arr: np.bool[:, :]
    :return: np.int[:, :]
    """
    return np.column_stack((x_vec_arr * 1,
                            np.ones(len(x_vec_arr), dtype=int) * -1,
                            y_vec_arr * 1))


def get_x_y_arr_2(data_train: DataVecXy, data_check: DataVecXy) -> np.ndarray:
    """
    :return: np.int[:, :]
    """
    arr_train = np.column_stack((data_train.x_vec_arr.astype(int),
                                 np.ones(len(data_train.x_vec_arr), dtype=int) * -1,
                                 data_train.y_arr.astype(int)))
    arr_check = np.column_stack((data_check.x_vec_arr.astype(int),
                                 np.ones(len(data_check.x_vec_arr), dtype=int) * -1,
                                 data_check.y_arr.astype(int)))
    arr_all = np.vstack((arr_train,
                         np.ones(arr_train.shape[1], dtype=int) * -1,
                         arr_check))
    return arr_all


def double_test():
    start_test = time.time()
    # подготавливаем данные
    count_x = 5
    data_bool = DataExampleBool(count_x)
    # сохраняем изначальные данные
    data_bool_x_copy = data_bool.x_vec_arr.copy()
    data_bool_y_copy = data_bool.y_vec_arr.copy()
    key = data_bool.y_vec_arr[0] * 1
    data_bool.shuffle()  # перемешиваем данные
    data_vec = DataVecXy()
    data_vec.set_by_bool_all(data_bool.x_vec_arr, data_bool.y_vec_arr, 0)
    data_train, data_check = separate(data_vec)
    # подготавливаем настройки
    settings = SettingsTrain()
    settings.the_type = "adam"
    settings.eta = 1.0
    settings.epochs = 100
    neuron = DataNeuron(count_x)
    # сохраняем начальные параметры нейрона
    weights_old = neuron.weights.copy()
    bias_old = neuron.bias
    # обучаем и получаем историю
    loss_arr_train, loss_arr_check = train(neuron, data_train, data_check, settings)
    print("Время повторного теста", time.time() - start_test, "секунд")
    print("key =", key)
    print(f"data:\n", get_x_y_arr(data_bool_x_copy, data_bool_y_copy))
    print(f"data prepare:\n", get_x_y_arr_2(data_train, data_check))
    print(f"train ({len(data_train.y_arr)}):\n", loss_arr_train.arr_mean_square)
    print(f"check ({len(data_check.y_arr)}):\n", loss_arr_check.arr_mean_square)
    print("weights_old:\n", weights_old)
    print("weights_now:\n", neuron.weights)
    print("bias_old =", bias_old)
    print("bias_now =", neuron.bias)


def get_separate_count(count: int, first_ratio: float) -> tuple[int, int]:
    edge_id = math.ceil(count * first_ratio)  # ceil - верхнее округление
    if edge_id == count:
        edge_id -= 1
    return edge_id, count - edge_id


def hot_simple_net_test():
    start_hot = time.time()
    data_bool = DataExampleBool(2)
    key = data_bool.get_y()[0] * 1
    data_bool.shuffle()  # перемешиваем данные
    # подготавливаем настройки
    settings = SettingsTrain()
    settings.the_type = "sgd"
    settings.eta = 10.0
    net = NeuroNet(2)
    loss_train, loss_check = net.train(data_bool, settings)
    print("Время теста", time.time() - start_hot, "секунд")
    print("key:", key)
    count_train, count_check = get_separate_count(len(data_bool.get_y()), settings.train_separate)
    print(f"len train = {count_train}; len check = {count_check}")
    print("mmse train:", np.mean(loss_train, axis=0))
    print("mmse check:", np.mean(loss_check, axis=0))


def main_simple_net_test():
    start_main = time.time()
    data_bool = DataExampleBool(6)
    key = data_bool.get_y()[0] * 1
    data_bool.shuffle()  # перемешиваем данные
    # подготавливаем настройки
    settings = SettingsTrain()
    settings.epochs = 300
    settings.the_type = "sgd"
    settings.eta = 10.0
    net = NeuroNet(6)
    loss_train, loss_check = net.train(data_bool, settings)
    print("Время main ", time.time() - start_main, "секунд")
    print("key:", key)
    count_train, count_check = get_separate_count(len(data_bool.get_y()), settings.train_separate)
    print(f"len train = {count_train}; len check = {count_check}")
    print("mmse train:", np.mean(loss_train, axis=0))
    print("mmse check:", np.mean(loss_check, axis=0))
    print(net.neurons[0].weights, net.neurons[0].bias)


def hot_net_test():
    start_hot = time.time()
    # net = NeuroNetHideUsual(2, 2, 2)
    net = NeuroNetHideBin(2, 2, 2)
    test_data = DataExampleFloat(2, "xor")
    key = test_data.get_y()[0]
    test_data.shuffle()
    settings = SettingsTrain()
    settings.the_type = "adam"
    data_train, data_check = NeuroNetHide.separate_data(test_data, settings.train_separate)
    # net.test_derivative(data_train)
    loss_train, loss_check = net.train(test_data, settings)
    print("Время теста", time.time() - start_hot, "секунд")
    print("key:", key)
    print(f"len train = {len(data_train.get_y())}; len check = {len(data_check.get_y())}")
    print("mmse train:", loss_train)
    print("mmse check:", loss_check)
    # net.test_derivative(data_train)


def main_net_test():
    start_main = time.time()
    # net = NeuroNetHideUsual(6, 6, 6)
    net = NeuroNetHideBin(6, 6, 6)
    test_data = DataExampleFloat(6, "xor")
    key = test_data.get_y()[0]
    test_data.shuffle()
    settings = SettingsTrain()
    settings.the_type = "adam"
    settings.epochs = 300
    data_train, data_check = NeuroNetHide.separate_data(test_data, settings.train_separate)
    # net.test_derivative(data_train)
    loss_train, loss_check = net.train(test_data, settings)
    print("Время main ", time.time() - start_main, "секунд")
    print("key:", key)
    print(f"len train = {len(data_train.get_y())}; len check = {len(data_check.get_y())}")
    print("mmse train:", loss_train)
    print("mmse check:", loss_check)
    # net.test_derivative(data_train)


def main():
    # проверка работоспособности и компиляция jit
    # runner = unittest.TextTestRunner(verbosity=2)
    # runner.run(get_test_suites())
    # time.sleep(0.1)  # немного ждем, чтоб поток успел вывести текст
    # проверка компиляции
    # double_test()

    np.set_printoptions(suppress=True, precision=4)
    hot_net_test()
    # hot_simple_net_test()
    print()
    print("#" * 100)
    print()
    np.set_printoptions(suppress=True, precision=4)
    main_net_test()
    # main_simple_net_test()


if __name__ == "__main__":
    main()
