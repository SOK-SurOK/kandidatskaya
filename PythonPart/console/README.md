Если указывать сигнатуру: `@njit(nb.int64(nb.uint8[:], nb.uint8[:]))`, то
компиляция будет происходить в момент,
когда питон парсит определение функции,
и первый запуск будет уже быстрым.
jitclass компилируется только при запуске

P.S. При указании сигнатуры массивов лучше задать явно способ чередования строк/столбцов:
чтобы numba не раздумывала 'C' (си) это или 'A'(автораспознавание си/фортран) — почему-то
это влияет на быстродействие даже для одномерных массивов,
для этого есть вот такой оригинальный синтаксис:
uint8[:,:] это 'A' (автоопределение),
nb.uint8[:, ::1] – это 'C' (си),
np.uint8[::1, :] – это 'F' (фортран).

Пример (полный):  
`@nb.njit(nb.int8[:, ::1](nb.int8[:, ::1]), fastmath=True, locals={'the_type': nb.int8}, cache=True, parallel = False)`

### unittest.TextTestRunner verbosity:
* 0 (quiet): you just get the total numbers of tests executed and the global result
* 1 (default): you get the same plus a dot for every successful test or a F for every failure
* 2 (verbose): you get the help string of every test and the result