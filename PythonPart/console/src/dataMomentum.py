import numba as nb
import numpy as np
from numba.experimental import jitclass


@jitclass([("mu_weights", nb.float_[::1])])
class DataMomentum:
    mu_weights: np.ndarray
    mu_bias: float

    def __init__(self, length: int):
        self.mu_weights = np.zeros(length, dtype=nb.float_)
        self.mu_bias = 0.0
