from typing import List

from src.dataLoss import DataLossArr


class DataTrainOut:
    loss_train: List[DataLossArr]
    loss_check: List[DataLossArr]
