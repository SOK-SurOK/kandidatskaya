import math

import numba as nb
import numpy as np

from src.dataAdam import DataAdam
from src.dataLoss import DataLoss, DataLossArr
from src.dataMomentum import DataMomentum
from src.dataNeuron import DataNeuron
from src.dataVecXy import DataVecXy
from src.settingsTrain import SettingsTrain


@nb.njit
def loss_epoch(neuron: DataNeuron, data: DataVecXy) -> DataLoss:
    count_data = len(data.y_arr)
    loss_arr_abs = np.zeros(count_data)
    loss_arr_relative = np.zeros(count_data)
    for n in range(count_data):
        y = y_out(neuron, data.x_vec_arr[n])
        loss_abs = abs(y - data.y_arr[n])
        loss_arr_abs[n] = loss_abs
        # «Относительная процентная разница», или RPD:
        relative_divider = data.y_arr[n] if data.y_arr[n] != 0 else 1
        loss_arr_relative[n] = loss_abs / relative_divider
    loss = DataLoss()
    loss.the_mean = np.mean(loss_arr_abs)
    loss.the_mean_square = np.mean(loss_arr_abs ** 2)
    loss.the_mean_relative = np.mean(loss_arr_relative)
    loss.the_min = np.min(loss_arr_abs)
    loss.the_max = np.max(loss_arr_abs)
    return loss


@nb.njit
def separate(data: DataVecXy, first_ratio: float = 0.8) -> tuple[DataVecXy, DataVecXy]:
    edge_id = math.ceil(len(data.y_arr) * first_ratio)  # ceil - верхнее округление
    if edge_id == len(data.y_arr):
        edge_id -= 1
    first = DataVecXy()
    first.set_by_separate(data.x_vec_arr[:edge_id], data.y_arr[:edge_id])
    second = DataVecXy()
    second.set_by_separate(data.x_vec_arr[edge_id:], data.y_arr[edge_id:])
    return first, second


########################################################################################################################

@nb.njit(nb.float_(nb.float_, nb.float_))
def sigmoid(value: float, lymbda: float) -> float:
    return 1 / (1 + np.exp(-1 * lymbda * value))


@nb.njit
def y_out(neuron: DataNeuron, x_vec: np.ndarray) -> float:
    """
    :param neuron: DataNeuron
    :param x_vec: nb.float_[:]
    """
    return sigmoid(np.dot(x_vec, neuron.weights) + neuron.bias, neuron.lymbda)


@nb.njit
def get_derivatives(neuron: DataNeuron, data: DataVecXy) -> tuple[float, np.ndarray]:
    """
    :return: tuple[float, nb.float_[:]]
    """
    count_train = len(data.y_arr)
    count_weights = len(neuron.weights)
    n_derivative_q_b_clear = np.zeros(count_train)
    for n in range(count_train):
        y = y_out(neuron, data.x_vec_arr[n])
        n_derivative_q_b_clear[n] = y * (1 - y) * (y - data.y_arr[n])
    n_derivative_q_b = ((2 * neuron.lymbda) / count_train) * n_derivative_q_b_clear
    derivative_q_b = float(np.sum(n_derivative_q_b))
    derivative_q_w = np.zeros(count_weights)
    for i in range(count_weights):
        derivative_q_w[i] = np.sum(n_derivative_q_b * data.x_vec_arr[:, i])
    return derivative_q_b, derivative_q_w


@nb.njit
def q_simple(neuron: DataNeuron, data: DataVecXy) -> float:
    count_train = len(data.y_arr)
    q_clear = np.zeros(count_train)
    for n in range(count_train):
        y = y_out(neuron, data.x_vec_arr[n])
        q_clear[n] = (y - data.y_arr[n]) ** 2
    return float(np.mean(q_clear))


@nb.njit
def get_derivatives_simple(neuron: DataNeuron, data: DataVecXy, delta: float) -> tuple[float, np.ndarray]:
    """
    :return: tuple[float, nb.float_[:]]
    """
    count_weights = len(neuron.weights)
    neuron_q = q_simple(neuron, data)  # один раз считаем
    neuron_delta = DataNeuron(count_weights)
    neuron_delta.lymbda = neuron.lymbda  # не меняется
    # подготавливаем данные нейрона для bias
    neuron_delta.weights = neuron.weights.copy()
    neuron_delta.bias = neuron.bias + delta
    derivative_q_b = (q_simple(neuron_delta, data) - neuron_q) / delta
    neuron_delta.bias = neuron.bias  # возвращаем на место
    # подготавливаем данные нейрона для weights
    derivative_q_w = np.zeros(count_weights)
    for i in range(count_weights):
        neuron_delta.weights = neuron.weights.copy()
        neuron_delta.weights[i] += delta
        derivative_q_w[i] = (q_simple(neuron_delta, data) - neuron_q) / delta
    return derivative_q_b, derivative_q_w


@nb.njit
def train_sgd(neuron: DataNeuron, data: DataVecXy, eta: float = 0.1):
    derivative_q_b, derivative_q_w = get_derivatives(neuron, data)
    neuron.bias -= (eta * derivative_q_b)
    neuron.weights -= (eta * derivative_q_w)


@nb.njit
def train_momentum(neuron: DataNeuron, data: DataVecXy,
                   data_momentum: DataMomentum,
                   settings: SettingsTrain):
    derivative_q_b, derivative_q_w = get_derivatives(neuron, data)
    data_momentum.mu_weights *= settings.gamma
    data_momentum.mu_weights += settings.eta * derivative_q_w
    data_momentum.mu_bias = settings.gamma * data_momentum.mu_bias + settings.eta * derivative_q_b
    neuron.bias -= data_momentum.mu_bias
    neuron.weights -= data_momentum.mu_weights


@nb.njit
def train_adam(neuron: DataNeuron, data: DataVecXy,
               data_adam: DataAdam,
               t: int, settings: SettingsTrain):
    derivative_q_b, derivative_q_w = get_derivatives(neuron, data)
    # обновляем конфигурацию
    data_adam.m_weights *= settings.beta1
    data_adam.m_weights += (1 - settings.beta1) * derivative_q_w
    data_adam.m_bias = settings.beta1 * data_adam.m_bias + (1 - settings.beta1) * derivative_q_b
    data_adam.nu_weights *= settings.beta2
    data_adam.nu_weights += (1 - settings.beta2) * (derivative_q_w ** 2)
    data_adam.nu_bias = settings.beta2 * data_adam.nu_bias + (1 - settings.beta2) * (derivative_q_b ** 2)
    # hat
    hat_m_weights = data_adam.m_weights / (1 - settings.beta1 ** t)
    hat_m_bias = data_adam.m_bias / (1 - settings.beta1 ** t)
    hat_nu_weights = data_adam.nu_weights / (1 - settings.beta2 ** t)
    hat_nu_bias = data_adam.nu_bias / (1 - settings.beta2 ** t)
    # обновляем веса
    neuron.weights -= (settings.eta * hat_m_weights) / (np.sqrt(hat_nu_weights) + 1e-6)
    neuron.bias -= (settings.eta * hat_m_bias) / (np.sqrt(hat_nu_bias) + 1e-6)


@nb.njit
def train(neuron: DataNeuron,
          data_train: DataVecXy,
          data_check: DataVecXy,
          settings: SettingsTrain) -> tuple[DataLossArr, DataLossArr]:
    """
    :return: loss_arr_train, loss_arr_check
    """
    # подготавливаем
    loss_arr_train = DataLossArr(settings.epochs)
    loss_arr_check = DataLossArr(settings.epochs)
    length = neuron.weights.shape[0]
    data_momentum = DataMomentum(length)
    data_adam = DataAdam(length)
    # обучение
    for e in range(settings.epochs):
        if settings.the_type == "sgd":
            train_sgd(neuron, data_train, settings.eta)
        elif settings.the_type == "momentum":
            train_momentum(neuron, data_train, data_momentum, settings)
        elif settings.the_type == "adam":
            train_adam(neuron, data_train, data_adam, e + 1, settings)
        else:
            raise Exception("unknown train type")
        neuron.binarize()
        # тестируем
        loss_arr_train.update(e, loss_epoch(neuron, data_train))
        loss_arr_check.update(e, loss_epoch(neuron, data_check))
    return loss_arr_train, loss_arr_check
