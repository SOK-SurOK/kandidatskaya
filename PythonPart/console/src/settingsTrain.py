from numba.experimental import jitclass


@jitclass
class SettingsTrain:
    the_type: str
    train_separate: float
    epochs: int
    eta: float
    gamma: float
    beta1: float
    beta2: float

    def __init__(self):
        self.the_type = ""
        self.epochs = 2
        self.eta = 0.1
        self.gamma = 0.9
        self.beta1 = 0.9
        self.beta2 = 0.9
        self.train_separate = 0.8
