from typing import List, Tuple

import numpy as np
from numba.experimental import jitclass
from numba.typed import List as NumbaList

import src.neuro as neuro
from src.dataExample import DataExampleBool
from src.dataLoss import DataLossArr
from src.dataNeuron import DataNeuron
from src.dataVecXy import DataVecXy
from src.settingsTrain import SettingsTrain


@jitclass
class NeuroNet:
    neurons: List[DataNeuron]

    def __init__(self, count_neuron: int):
        self.neurons = NumbaList([DataNeuron(count_neuron) for _ in range(count_neuron)])

    def train(self,
              data: DataExampleBool,
              settings: SettingsTrain) -> Tuple[np.ndarray, np.ndarray]:
        count_neurons = data.y_vec_arr.shape[1]
        loss_train, loss_check = np.zeros((count_neurons, settings.epochs)), np.zeros((count_neurons, settings.epochs))
        for i_y in range(count_neurons):
            # подготавливаем обучение
            data_vec = DataVecXy()
            data_vec.set_by_bool_all(data.x_vec_arr, data.y_vec_arr, i_y)
            data_train, data_check = neuro.separate(data_vec, settings.train_separate)
            # запуск обучения
            l_train, l_check = neuro.train(self.neurons[i_y], data_train, data_check, settings)
            loss_train[i_y] += l_train.arr_mean_square
            loss_check[i_y] += l_check.arr_mean_square
        return loss_train, loss_check
