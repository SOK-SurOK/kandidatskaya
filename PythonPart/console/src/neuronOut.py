from typing import List

import numba as nb
import numpy as np
from numba.experimental import jitclass

from src.dataExample import DataExample
from src.neuron import Neuron


@jitclass([("weights", nb.float_[::1])])
class NeuronOut(Neuron):

    def __init__(self, weights_count: int, the_id: int):
        self.weights = np.random.sample(weights_count).astype(nb.float_) * 2 - 1
        self.bias = np.random.sample() * 2.0 - 1.0
        self.lymbda = 1.0
        self.the_id = the_id  # the_id = k

    def get_derivatives(self, data: DataExample, neurons_hide: List[Neuron],
                        big_k: int) -> tuple[float, np.ndarray]:
        """
        :return: tuple[float, nb.float_[:]]
        """
        big_n = len(data.get_y())
        big_j = len(neurons_hide)
        n_derivative_q_b_clear = np.zeros(big_n)
        hide_out = np.zeros((big_n, big_j), dtype=nb.float_)
        for n in range(big_n):
            for j in range(big_j):
                hide_out[n, j] = neurons_hide[j].out(data.get_x()[n])
            y = self.out(hide_out[n])
            n_derivative_q_b_clear[n] = y * (1 - y) * (y - data.get_y()[n, self.the_id])
        n_derivative_q_b = ((2 * self.lymbda) / (big_n * big_k)) * n_derivative_q_b_clear
        derivative_q_b = float(np.sum(n_derivative_q_b))
        # теперь w
        derivative_q_w = np.zeros(big_j, dtype=nb.float_)
        for j in range(big_j):
            derivative_q_w[j] = np.sum(n_derivative_q_b * hide_out[:, j])
        return derivative_q_b, derivative_q_w

    def get_derivatives_simple(self, data: DataExample, neurons_hide: List[Neuron],
                               big_k: int) -> tuple[float, np.ndarray]:
        """
        :return: tuple[float, nb.float_[:]]
        """
        big_n = len(data.get_y())
        big_j = len(self.weights)
        delta = 1e-4
        safe_weights = self.weights.copy()
        safe_bias = self.bias
        n_derivative_q_b_clear = np.zeros(big_n, dtype=nb.float_)
        n_derivative_q_w_clear = np.zeros((big_n, big_j), dtype=nb.float_)
        for n in range(big_n):
            hide_out = np.zeros(big_j, dtype=nb.float_)
            for j in range(big_j):
                hide_out[j] = neurons_hide[j].out(data.get_x()[n])
            y = self.out(hide_out)
            quality = (y - data.get_y()[n, self.the_id]) ** 2
            # вычисляем производные по w
            for j in range(big_j):
                self.weights[j] += delta
                quality_delta = (self.out(hide_out) - data.get_y()[n, self.the_id]) ** 2
                self.weights[j] = safe_weights[j]
                n_derivative_q_w_clear[n, j] = (quality_delta - quality) / delta
            # вычисляем производную по b
            self.bias += delta
            quality_delta = (self.out(hide_out) - data.get_y()[n, self.the_id]) ** 2
            self.bias = safe_bias
            n_derivative_q_b_clear[n] = (quality_delta - quality) / delta
        derivative_q_b = float(np.sum(n_derivative_q_b_clear)) / (big_n * big_k)
        derivative_q_w = np.zeros(big_j, dtype=nb.float_)
        for j in range(big_j):
            derivative_q_w[j] = np.sum(n_derivative_q_w_clear[:, j]) / (big_n * big_k)
        return derivative_q_b, derivative_q_w
