import numba as nb
import numpy as np
from numba.experimental import jitclass


@jitclass
class DataLoss:
    the_max: float
    the_min: float
    the_mean: float
    the_mean_relative: float
    the_mean_square: float

    def __init__(self):
        self.the_max = 0
        self.the_min = 0
        self.the_mean = 0
        self.the_mean_relative = 0
        self.the_mean_square = 0


@jitclass([
    ("arr_max", nb.float_[::1]),
    ("arr_min", nb.float_[::1]),
    ("arr_mean", nb.float_[::1]),
    ("arr_mean_relative", nb.float_[::1]),
    ("arr_mean_square", nb.float_[::1]),
])
class DataLossArr:
    arr_max: np.ndarray
    arr_min: np.ndarray
    arr_mean: np.ndarray
    arr_mean_relative: np.ndarray
    arr_mean_square: np.ndarray

    def __init__(self, length: int):
        self.arr_max = np.zeros(length, dtype=nb.float_)
        self.arr_min = np.zeros(length, dtype=nb.float_)
        self.arr_mean = np.zeros(length, dtype=nb.float_)
        self.arr_mean_relative = np.zeros(length, dtype=nb.float_)
        self.arr_mean_square = np.zeros(length, dtype=nb.float_)

    def update(self, index: int, loss: DataLoss):
        self.arr_max[index] = loss.the_max
        self.arr_min[index] = loss.the_min
        self.arr_mean[index] = loss.the_mean
        self.arr_mean_relative[index] = loss.the_mean_relative
        self.arr_mean_square[index] = loss.the_mean_square
