import math
from typing import List

import numba as nb
import numpy as np

from src.dataAdam import DataAdam
from src.dataExample import DataExample, DataExampleFloatInit
from src.dataMomentum import DataMomentum
from src.neuron import Neuron
from src.neuronHide import NeuronHide
from src.neuronOut import NeuronOut
from src.settingsTrain import SettingsTrain


class NeuroNetHide:
    neurons_out: List[NeuronOut]
    neurons_hide: List[NeuronHide]
    hide_work: np.ndarray

    def out(self, in_vec: np.ndarray) -> np.ndarray:
        """
        :param in_vec: nb.float_[:]
        :return: nb.float_[::1]
        """
        for j in range(len(self.neurons_hide)):
            self.hide_work[j] = self.neurons_hide[j].out(in_vec)
        out_work = np.zeros(len(self.neurons_out), dtype=nb.float_)
        for k in range(len(self.neurons_out)):
            out_work[k] = self.neurons_out[k].out(self.hide_work)
        return out_work

    def test_derivative(self, test_data: DataExample):
        big_k = len(self.neurons_out)
        for neuron_hide in self.neurons_hide:
            d_a_func, d_v_func = neuron_hide.get_derivatives(test_data, self.neurons_out, self.neurons_hide)
            d_a_lim, d_v_lim = neuron_hide.get_derivatives_simple(test_data, self.neurons_out, self.neurons_hide)
            print(neuron_hide)
            print("d_a_func:")
            print(d_a_func)
            print(f"d_a_lim:")
            print(d_a_lim)
            print("d_v_func:")
            print(d_v_func)
            print("d_v_lim:")
            print(d_v_lim)
            print()
        for neuron_out in self.neurons_out:
            d_b_func, d_w_func = neuron_out.get_derivatives(test_data, self.neurons_hide, big_k)
            d_b_lim, d_w_lim = neuron_out.get_derivatives_simple(test_data, self.neurons_hide, big_k)
            print(neuron_out)
            print("d_b_func:")
            print(d_b_func)
            print(f"d_b_lim:")
            print(d_b_lim)
            print("d_w_func:")
            print(d_w_func)
            print("d_w_lim:")
            print(d_w_lim)
            print()

    @staticmethod
    def separate_data(data: DataExample, first_ratio: float) -> tuple[DataExample, DataExample]:
        edge_id = math.ceil(len(data.get_y()) * first_ratio)  # ceil - верхнее округление
        if edge_id == len(data.get_y()):
            edge_id -= 1
        first = DataExampleFloatInit(data.get_x()[:edge_id], data.get_y()[:edge_id])
        second = DataExampleFloatInit(data.get_x()[edge_id:], data.get_y()[edge_id:])
        return first, second

    def get_epoch_loss(self, data: DataExample) -> float:
        big_n = len(data.get_y())
        loss_mean_square = np.zeros(big_n)
        for n in range(big_n):
            out = self.out(data.get_x()[n])
            loss_abs = np.abs(out - data.get_y()[n])
            loss_mean_square[n] = np.mean(loss_abs ** 2)
        return float(np.mean(loss_mean_square))

    @staticmethod
    def train_sgd(neuron: Neuron, d_bias: float, d_weights: np.ndarray, eta: float):
        neuron.bias -= eta * d_bias
        neuron.weights -= eta * d_weights

    @staticmethod
    def train_momentum(neuron: Neuron, d_bias: float, d_weights: np.ndarray, data_momentum: DataMomentum,
                       settings: SettingsTrain):
        data_momentum.mu_bias = settings.gamma * data_momentum.mu_bias + settings.eta * d_bias
        data_momentum.mu_weights *= settings.gamma
        data_momentum.mu_weights += settings.eta * d_weights
        neuron.bias -= data_momentum.mu_bias
        neuron.weights -= data_momentum.mu_weights

    @staticmethod
    def train_adam(neuron: Neuron, d_bias: float, d_weights: np.ndarray, data_adam: DataAdam,
                   t: int, settings: SettingsTrain):
        # обновляем конфигурацию m
        data_adam.m_bias = settings.beta1 * data_adam.m_bias + (1 - settings.beta1) * d_bias
        data_adam.m_weights *= settings.beta1
        data_adam.m_weights += (1 - settings.beta1) * d_weights
        # обновляем конфигурацию nu
        data_adam.nu_bias = settings.beta2 * data_adam.nu_bias + (1 - settings.beta2) * (d_bias ** 2)
        data_adam.nu_weights *= settings.beta2
        data_adam.nu_weights += (1 - settings.beta2) * (d_weights ** 2)
        # hat
        hat_m_weights = data_adam.m_weights / (1 - settings.beta1 ** t)
        hat_m_bias = data_adam.m_bias / (1 - settings.beta1 ** t)
        hat_nu_weights = data_adam.nu_weights / (1 - settings.beta2 ** t)
        hat_nu_bias = data_adam.nu_bias / (1 - settings.beta2 ** t)
        # обновляем веса
        neuron.weights -= (settings.eta * hat_m_weights) / (np.sqrt(hat_nu_weights) + 1e-6)
        neuron.bias -= (settings.eta * hat_m_bias) / (np.sqrt(hat_nu_bias) + 1e-6)

    def train(self, data: DataExample, settings: SettingsTrain) -> tuple[np.ndarray, np.ndarray]:
        pass
