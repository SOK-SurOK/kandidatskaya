import numba as nb
import numpy as np
from numba.experimental import jitclass


@jitclass([("m_weights", nb.float_[::1]), ("nu_weights", nb.float_[::1])])
class DataAdam:
    m_weights: np.ndarray
    nu_weights: np.ndarray
    m_bias: float
    nu_bias: float

    def __init__(self, length: int):
        self.m_weights = np.zeros(length, dtype=nb.float_)
        self.nu_weights = np.zeros(length, dtype=nb.float_)
        self.m_bias = 0.0
        self.nu_bias = 0.0
