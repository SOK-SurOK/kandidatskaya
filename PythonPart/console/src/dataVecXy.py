import numba as nb
import numpy as np
from numba.experimental import jitclass


@jitclass([("x_vec_arr", nb.float_[:, ::1]), ("y_arr", nb.float_[::1])])
class DataVecXy:
    x_vec_arr: np.ndarray
    y_arr: np.ndarray

    def __init__(self):  # для jitclass обязательно переопределять
        pass

    def set_by_bool_all(self, x_vec_arr: np.ndarray, y_vec_arr: np.ndarray, y_id: int):
        """
        :param x_vec_arr: nb.bool_[:, :]
        :param y_vec_arr: nb.bool_[:, :]
        :param y_id: int
        """
        if len(x_vec_arr) != len(y_vec_arr):
            raise Exception("Нужно одинаковое количество примеров и ответов")
        self.x_vec_arr = x_vec_arr.astype(nb.float_)
        self.y_arr = y_vec_arr[:, y_id].astype(nb.float_)

    def set_by_separate(self, x_vec_arr: np.ndarray, y_arr: np.ndarray):
        """
        :param x_vec_arr: nb.float_[:, :]
        :param y_arr: nb.float_[:]
        """
        if len(x_vec_arr) != len(y_arr):
            raise Exception("Нужно одинаковое количество примеров и ответов")
        self.x_vec_arr = x_vec_arr.astype(nb.float_)
        self.y_arr = y_arr.astype(nb.float_)
