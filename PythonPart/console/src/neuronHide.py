from typing import List

import numpy as np
from numba.experimental import jitclass
import numba as nb
from src.dataExample import DataExample
from src.neuron import Neuron


@jitclass([("weights", nb.float_[::1])])
class NeuronHide(Neuron):

    def __init__(self, weights_count: int, the_id: int):
        self.weights = np.random.sample(weights_count).astype(nb.float_) * 2 - 1
        self.bias = np.random.sample() * 2.0 - 1.0
        self.lymbda = 1.0
        self.the_id = the_id  # the_id = j

    def get_derivatives(self, data: DataExample, neurons_out: List[Neuron],
                        neurons_hide: List[Neuron]) -> tuple[float, np.ndarray]:
        """
        :return: tuple[float, nb.float_[:]]
        """
        big_n = len(data.get_y())
        big_k = len(neurons_out)
        big_j = len(neurons_hide)
        n_derivative_q_a_clear = np.zeros(big_n, dtype=nb.float_)
        for n in range(big_n):
            hide_out = np.zeros(big_j, dtype=nb.float_)
            for j in range(big_j):
                hide_out[j] = neurons_hide[j].out(data.get_x()[n])
            sum_k = 0.0
            for k in range(big_k):
                y = neurons_out[k].out(hide_out)
                sum_k += y * (1 - y) * (y - data.get_y()[n, k]) * neurons_out[k].weights[self.the_id]
            n_derivative_q_a_clear[n] = hide_out[self.the_id] * (1 - hide_out[self.the_id]) * sum_k
        n_derivative_q_a = (2 * n_derivative_q_a_clear * self.lymbda ** 2) / (big_n * big_k)
        derivative_q_a = float(np.sum(n_derivative_q_a))
        # теперь v
        big_i = len(self.weights)
        derivative_q_v = np.zeros(big_i, dtype=nb.float_)
        for i in range(big_i):
            derivative_q_v[i] = np.sum(n_derivative_q_a * data.get_x()[:, i])
        return derivative_q_a, derivative_q_v

    def get_derivatives_simple(self, data: DataExample, neurons_out: List[Neuron],
                               neurons_hide: List[Neuron]) -> tuple[float, np.ndarray]:
        """
        :return: tuple[float, nb.float_[:]]
        """
        big_n = len(data.get_y())
        big_k = len(neurons_out)
        big_i = len(self.weights)
        big_j = len(neurons_hide)
        delta = 1e-4
        safe_weights = self.weights.copy()
        safe_bias = self.bias
        derivative_q_a_clear = 0.0
        derivative_q_v_clear = np.zeros(big_i, dtype=nb.float_)
        for n in range(big_n):
            hide_out = np.zeros(big_j, dtype=nb.float_)
            for j in range(big_j):
                hide_out[j] = neurons_hide[j].out(data.get_x()[n])
            safe_hide_out = hide_out.copy()
            for k in range(big_k):
                quality = (neurons_out[k].out(hide_out) - data.get_y()[n, k]) ** 2
                # вычисляем производные по i
                for i in range(big_i):
                    self.weights[i] += delta
                    hide_out[self.the_id] = self.out(data.get_x()[n])
                    self.weights[i] = safe_weights[i]
                    quality_delta = (neurons_out[k].out(hide_out) - data.get_y()[n, k]) ** 2
                    derivative_q_v_clear[i] += (quality_delta - quality) / delta
                self.bias += delta
                hide_out[self.the_id] = self.out(data.get_x()[n])
                self.bias = safe_bias
                quality_delta = (neurons_out[k].out(hide_out) - data.get_y()[n, k]) ** 2
                derivative_q_a_clear += (quality_delta - quality) / delta
                hide_out[self.the_id] = safe_hide_out[self.the_id]
        return derivative_q_a_clear / (big_n * big_k), derivative_q_v_clear / (big_n * big_k)
