import numpy as np
import numba as nb


class Neuron:
    the_id: int
    weights: np.ndarray
    bias: float
    lymbda: float

    def out(self, in_vec: np.ndarray) -> float:
        """
        :param in_vec: nb.float_[:]
        """
        return 1 / (1 + np.exp(-1 * self.lymbda * (np.dot(in_vec, self.weights) + self.bias)))

    def binarize(self):
        np.around(self.weights, 0, self.weights)
        self.weights = self.weights.astype(nb.bool_).astype(nb.float_)
