import math
from typing import List

import numba as nb
import numpy as np
from numba.experimental import jitclass
from numba.typed import List as NumbaList

from src.dataAdam import DataAdam
from src.dataExample import DataExample, DataExampleFloatInit
from src.dataMomentum import DataMomentum
from src.neuroNetHide import NeuroNetHide
from src.neuron import Neuron
from src.neuronHide import NeuronHide
from src.neuronOut import NeuronOut
from src.settingsTrain import SettingsTrain


@jitclass([("hide_work", nb.float_[::1])])
class NeuroNetHideUsual(NeuroNetHide):

    def __init__(self, size_x: int, count_h: int, size_y: int):
        self.neurons_out = NumbaList([NeuronOut(count_h, k) for k in range(size_y)])
        self.neurons_hide = NumbaList([NeuronHide(size_x, j) for j in range(count_h)])
        self.hide_work = np.zeros(count_h, dtype=nb.float_)

    def train(self, data: DataExample, settings: SettingsTrain) -> tuple[np.ndarray, np.ndarray]:
        big_i = len(self.neurons_hide[0].weights)
        big_j = len(self.neurons_hide)
        big_k = len(self.neurons_out)
        data_train, data_check = self.separate_data(data, settings.train_separate)
        loss_train, loss_check = np.zeros(settings.epochs), np.zeros(settings.epochs)
        # данные для обучения
        momentum_hide = NumbaList([DataMomentum(big_i) for _ in range(big_j)])
        adam_hide = NumbaList([DataAdam(big_i) for _ in range(big_j)])
        momentum_out = NumbaList([DataMomentum(big_j) for _ in range(big_k)])
        adam_out = NumbaList([DataAdam(big_j) for _ in range(big_k)])
        # эпохи
        for e in range(settings.epochs):
            # hidden layer
            for j in range(big_j):
                d_a, d_v = self.neurons_hide[j].get_derivatives(data_train, self.neurons_out, self.neurons_hide)
                if settings.the_type == "sgd":
                    self.train_sgd(self.neurons_hide[j], d_a, d_v, settings.eta)
                elif settings.the_type == "momentum":
                    self.train_momentum(self.neurons_hide[j], d_a, d_v, momentum_hide[j], settings)
                elif settings.the_type == "adam":
                    self.train_adam(self.neurons_hide[j], d_a, d_v, adam_hide[j], e + 1, settings)
                else:
                    raise Exception("unknown train type")
            # output layer
            for k in range(big_k):
                d_b, d_w = self.neurons_out[k].get_derivatives(data_train, self.neurons_hide, big_k)
                if settings.the_type == "sgd":
                    self.train_sgd(self.neurons_out[k], d_b, d_w, settings.eta)
                elif settings.the_type == "momentum":
                    self.train_momentum(self.neurons_out[k], d_b, d_w, momentum_out[k], settings)
                elif settings.the_type == "adam":
                    self.train_adam(self.neurons_out[k], d_b, d_w, adam_out[k], e + 1, settings)
                else:
                    raise Exception("unknown train type")
            loss_train[e] = self.get_epoch_loss(data_train)
            loss_check[e] = self.get_epoch_loss(data_check)
        return loss_train, loss_check
