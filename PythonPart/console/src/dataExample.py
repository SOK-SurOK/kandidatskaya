import numba as nb
import numpy as np
from numba.experimental import jitclass


@nb.njit(nb.bool_[:, ::1](nb.int_), fastmath=True)
def create_arr_all_bit(bit_count: int) -> np.ndarray:
    arr_len = 2 ** bit_count
    arr = np.zeros((arr_len, bit_count), dtype=nb.bool_)
    for j in range(bit_count):
        j_end = bit_count - j - 1
        skip = 2 ** j
        skip_acc = skip
        for i in range(arr_len):
            arr[i][j_end] = False if skip_acc > 0 else True
            skip_acc -= 1
            if skip_acc <= -skip:
                skip_acc = skip
    return arr


@nb.njit(nb.bool_[:, ::1](nb.bool_[:, ::1]), fastmath=True)
def create_arr_by_xor(x_vec_arr: np.ndarray) -> np.ndarray:
    return np.logical_xor(x_vec_arr, np.random.randint(0, 2, x_vec_arr.shape[1]))


class DataExample:
    x_vec_arr: np.ndarray
    y_vec_arr: np.ndarray

    def get_x(self) -> np.ndarray:
        return self.x_vec_arr

    def get_y(self) -> np.ndarray:
        return self.y_vec_arr

    def shuffle(self):
        seed = np.random.randint(0, 2 ** 32)
        np.random.seed(seed)
        np.random.shuffle(self.x_vec_arr)
        np.random.seed(seed)
        np.random.shuffle(self.y_vec_arr)


@jitclass([("x_vec_arr", nb.bool_[:, ::1]), ("y_vec_arr", nb.bool_[:, ::1])])
class DataExampleBool(DataExample):

    def __init__(self, x_vec_len: int, y_type: str = "xor"):
        if y_type == "xor":
            self.x_vec_arr = create_arr_all_bit(x_vec_len)
            self.y_vec_arr = create_arr_by_xor(self.x_vec_arr)
        else:
            raise Exception("unknown y_type")


@jitclass([("x_vec_arr", nb.float_[:, ::1]), ("y_vec_arr", nb.float_[:, ::1])])
class DataExampleFloat(DataExample):

    def __init__(self, x_vec_len: int, y_type: str = "xor"):
        if y_type == "xor":
            x_vec_arr = create_arr_all_bit(x_vec_len)
            self.y_vec_arr = create_arr_by_xor(x_vec_arr).astype(nb.float_)
            self.x_vec_arr = x_vec_arr.astype(nb.float_)
        else:
            raise Exception("unknown y_type")


@jitclass([("x_vec_arr", nb.float_[:, ::1]), ("y_vec_arr", nb.float_[:, ::1])])
class DataExampleFloatInit(DataExample):

    def __init__(self, x_vec_arr: np.ndarray, y_vec_arr: np.ndarray):
        self.x_vec_arr = x_vec_arr.astype(nb.float_)
        self.y_vec_arr = y_vec_arr.astype(nb.float_)
