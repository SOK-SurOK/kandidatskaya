import numba as nb
import numpy as np
from numba.experimental import jitclass


@jitclass([("weights", nb.float_[::1])])
class DataNeuron:
    weights: np.ndarray
    bias: float
    lymbda: float

    def __init__(self, w_len: int):
        self.weights = np.random.sample(w_len).astype(nb.float_) * 2 - 1
        self.bias = np.random.sample() * 2.0 - 1.0
        self.lymbda = 1.0
        self.binarize()

    def binarize(self):
        np.around(self.weights, 0, self.weights)
        self.weights = self.weights.astype(nb.bool_).astype(nb.float_)
        # self.bias = round(self.bias)
