import unittest
import numpy as np

from src.dataNeuron import DataNeuron


class TestDataNeuron(unittest.TestCase):
    const_1 = 2

    def test_init(self):
        obj = DataNeuron(self.const_1)
        self.assertEqual(obj.lymbda, 1.0)
        self.assertTrue(isinstance(obj.bias, float))
        self.assertTrue(isinstance(obj.weights[0], (np.float32, np.float64)))
