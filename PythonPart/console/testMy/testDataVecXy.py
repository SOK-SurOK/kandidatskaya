import unittest
import numpy as np

from src.dataVecXy import DataVecXy


class TestDataVecXy(unittest.TestCase):
    const_1 = 2
    const_2 = 3

    def test_set_by_bool_all(self):
        obj = DataVecXy()
        shape = (self.const_1, self.const_2)
        arr_bool_1 = np.random.randint(0, 2, shape, dtype=bool)
        arr_bool_2 = np.random.randint(0, 2, shape, dtype=bool)
        obj.set_by_bool_all(arr_bool_1, arr_bool_2, 0)
        self.assertEqual(obj.x_vec_arr.shape, shape)
        self.assertEqual(len(obj.y_arr), self.const_1)

    def test_set_by_separate(self):
        obj = DataVecXy()
        shape = (self.const_1, self.const_2)
        arr_bool_1 = np.random.randint(0, 2, shape).astype(float)
        arr_bool_2 = np.random.randint(0, 2, self.const_1).astype(float)
        obj.set_by_separate(arr_bool_1, arr_bool_2)
        self.assertEqual(obj.x_vec_arr.shape, shape)
        self.assertEqual(len(obj.y_arr), self.const_1)
