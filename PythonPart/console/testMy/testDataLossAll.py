import unittest
import numpy as np

from src.dataLoss import DataLoss, DataLossArr


class TestDataLossAll(unittest.TestCase):
    const_1 = 2

    def test_init(self):
        obj = DataLoss()
        objs = DataLossArr(self.const_1)
        self.assertEqual(obj.the_max, 0)
        self.assertEqual(len(objs.arr_max), self.const_1)
        self.assertTrue(isinstance(objs.arr_max, np.ndarray))
        self.assertTrue(isinstance(objs.arr_max[0], (np.float32, np.float64)))

    def test_update(self):
        obj = DataLoss()
        obj.the_max = 1
        objs = DataLossArr(self.const_1)
        objs_arr_max_old = objs.arr_max.copy()
        objs.update(0, obj)
        self.assertFalse(np.array_equal(objs_arr_max_old, objs.arr_max))
