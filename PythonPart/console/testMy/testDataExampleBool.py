import unittest
import numpy as np

from src.dataExample import DataExampleBool


class TestDataExampleBool(unittest.TestCase):
    const_1 = 3

    def test_init(self):
        obj = DataExampleBool(self.const_1)
        self.assertEqual(obj.x_vec_arr.shape[1], self.const_1)
        self.assertEqual(obj.y_vec_arr.shape[1], self.const_1)

    @unittest.skip
    def test_get_set(self):
        obj = DataExampleBool(self.const_1)
        old_x = obj.x_vec_arr.copy()
        old_y = obj.y_vec_arr.copy()
        obj.x_vec_arr = obj.x_vec_arr ^ True
        obj.y_vec_arr = obj.y_vec_arr ^ True
        self.assertFalse(np.array_equal(obj.x_vec_arr, old_x))
        self.assertFalse(np.array_equal(obj.y_vec_arr, old_y))

    def test_shuffle(self):
        obj = DataExampleBool(self.const_1)
        old_x = obj.x_vec_arr.copy()
        old_y = obj.y_vec_arr.copy()
        obj.shuffle()
        try:
            self.assertFalse(np.array_equal(obj.x_vec_arr, old_x))
            self.assertFalse(np.array_equal(obj.y_vec_arr, old_y))
        except AssertionError:
            print("Warning in", "test_shuffle", __file__)

    def test_shuffle_deep(self):
        obj = DataExampleBool(self.const_1)
        old_x = obj.x_vec_arr.copy()
        old_y = obj.y_vec_arr.copy()
        obj.shuffle()
        self.assertEqual(old_x.shape, obj.x_vec_arr.shape)
        self.assertEqual(old_y.shape, obj.y_vec_arr.shape)
        for i in range(len(old_x)):
            for j in range(len(obj.x_vec_arr)):
                if np.array_equal(old_x[i], obj.x_vec_arr[j]):
                    self.assertTrue(np.array_equal(old_y[i], obj.y_vec_arr[j]))
