import unittest

from src.dataExample import DataExampleBool
from src.neuroNet import NeuroNet
from src.settingsTrain import SettingsTrain


class TestNeuroNet(unittest.TestCase):
    const_1 = 3

    def test_train_net(self):
        # подготавливаем данные
        data_bool = DataExampleBool(self.const_1)
        data_bool.shuffle()  # перемешиваем данные
        # подготавливаем настройки
        settings = SettingsTrain()
        settings.the_type = "sgd"
        net = NeuroNet(self.const_1)
        loss_train, loss_check = net.train(data_bool, settings)
        print(loss_train)
        print(loss_check)
