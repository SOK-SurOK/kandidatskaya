import unittest

from src.dataExample import DataExampleBool
from src.dataLoss import DataLossArr
from src.dataNeuron import DataNeuron
from src.dataVecXy import DataVecXy
from src.neuro import train, separate, get_derivatives, get_derivatives_simple
from src.settingsTrain import SettingsTrain


class TestNeuron(unittest.TestCase):
    const_1 = 3

    def test_derivatives(self):
        data_bool = DataExampleBool(self.const_1)
        data_vec = DataVecXy()
        data_vec.set_by_bool_all(data_bool.x_vec_arr, data_bool.y_vec_arr, 0)
        neuron = DataNeuron(self.const_1)
        d_b, d_w = get_derivatives(neuron, data_vec)
        d_b_simple, d_w_simple = get_derivatives_simple(neuron, data_vec, 1e-4)  # оптимально delta=1e-4
        for i in range(len(d_w)):
            with self.subTest(i=i):
                self.assertAlmostEqual(d_w[i], d_w_simple[i], places=3)
        self.assertAlmostEqual(d_b, d_b_simple, places=3)

    def test_train_all(self):
        # подготавливаем данные
        data_bool = DataExampleBool(self.const_1)
        data_bool.shuffle()  # перемешиваем данные
        data_vec = DataVecXy()
        data_vec.set_by_bool_all(data_bool.x_vec_arr, data_bool.y_vec_arr, 0)
        data_train, data_check = separate(data_vec)
        # подготавливаем настройки
        settings = SettingsTrain()
        neuron = DataNeuron(self.const_1)
        # обучаем и получаем историю
        with self.subTest("sgd"):
            settings.the_type = "sgd"
            loss_arr_train, loss_arr_check = train(neuron, data_train, data_check, settings)
            self.assertIsInstance(loss_arr_train, DataLossArr)
            self.assertIsInstance(loss_arr_check, DataLossArr)
        with self.subTest("momentum"):
            settings.the_type = "momentum"
            loss_arr_train, loss_arr_check = train(neuron, data_train, data_check, settings)
            self.assertIsInstance(loss_arr_train, DataLossArr)
            self.assertIsInstance(loss_arr_check, DataLossArr)
        with self.subTest("adam"):
            settings.the_type = "adam"
            loss_arr_train, loss_arr_check = train(neuron, data_train, data_check, settings)
            self.assertIsInstance(loss_arr_train, DataLossArr)
            self.assertIsInstance(loss_arr_check, DataLossArr)
