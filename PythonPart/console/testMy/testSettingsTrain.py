import unittest

from src.settingsTrain import SettingsTrain


class TestSettingsTrain(unittest.TestCase):
    const_1 = "hi"
    const_2 = 2

    def test_init(self):
        obj = SettingsTrain()
        obj.the_type = self.const_1
        obj.eta = self.const_2
        self.assertEqual(obj.the_type, self.const_1)
        self.assertEqual(obj.eta, self.const_2)
